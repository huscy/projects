Feature: delete research units

	Scenario: delete research unit as admin user
		Given I am admin user

		When I try to delete a research unit

		Then I get status code 204

	Scenario: delete research unit with delete_researchunit permission
		Given I am admin user
		And I have delete_researchunit permission

		When I try to delete a research unit

		Then I get status code 204

	Scenario: delete research unit as normal user
		Given I am normal user

		When I try to delete a research unit

		Then I get status code 403

	Scenario: delete research unit as anonymous user
		Given I am anonymous user

		When I try to delete a research unit

		Then I get status code 403
