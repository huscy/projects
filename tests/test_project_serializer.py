import pytest
from model_bakery import baker

from huscy.projects.models import Membership
from huscy.projects.serializers import CreateProjectSerializer, ProjectSerializer

pytestmark = pytest.mark.django_db


def test_project_serializer_fields(rf, user, project):
    request = rf.get('/any/path/')
    request.user = user

    expected = dict(
        description=project.description,
        id=project.id,
        local_id=project.local_id,
        local_id_name=project.local_id_name,
        participating=False,
        principal_investigators=[],
        research_unit=project.research_unit.pk,
        research_unit_name=project.research_unit.name,
        title=project.title,
    )

    assert expected == ProjectSerializer(instance=project, context={'request': request}).data


def test_create_project_serializer_fields(rf, user, project):
    request = rf.get('/any/path/')
    request.user = user

    expected = dict(
        id=project.id,
        description=project.description,
        local_id=project.local_id,
        local_id_name=project.local_id_name,
        participating=False,
        research_unit=project.research_unit.pk,
        research_unit_name=project.research_unit.name,
        title=project.title,
    )

    assert expected == CreateProjectSerializer(instance=project, context={'request': request}).data


def test_participating_flag_with_member(rf, membership):
    request = rf.get('/any/path/')
    request.user = membership.user

    serializer = ProjectSerializer(instance=membership.project, context={'request': request})

    assert serializer.data['participating'] is True


def test_participating_flag_with_any_other_user(rf, user, project):
    request = rf.get('/any/path/')
    request.user = user

    serializer = ProjectSerializer(instance=project, context={'request': request})

    assert serializer.data['participating'] is False


def test_expose_principal_investigators(rf, user, project):
    principal_investigators = baker.make(
        Membership, project=project, role=Membership.Roles.PRINCIPAL_INVESTIGATOR, _quantity=2)

    request = rf.get('/any/path/')
    request.user = user

    expected = dict(
        description=project.description,
        id=project.id,
        local_id=project.local_id,
        local_id_name=project.local_id_name,
        participating=False,
        principal_investigators=[
            {'id': principal_investigators[0].user.pk,
             'full_name': principal_investigators[0].user.get_full_name()},
            {'id': principal_investigators[1].user.pk,
             'full_name': principal_investigators[1].user.get_full_name()}
        ],
        research_unit=project.research_unit.pk,
        research_unit_name=project.research_unit.name,
        title=project.title,
    )

    assert expected == ProjectSerializer(instance=project, context={'request': request}).data
