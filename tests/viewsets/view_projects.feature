Feature: view projects

	Scenario: list projects as admin user
		Given I am admin user

		When I try to list projects

		Then I get status code 200

	Scenario: list projects as staff user
		Given I am staff user

		When I try to list projects

		Then I get status code 200

	Scenario: list projects as normal user
		Given I am normal user

		When I try to list projects

		Then I get status code 200

	Scenario: list projects as anonymous user
		Given I am anonymous user

		When I try to list projects

		Then I get status code 403

	Scenario: retrieve project as admin user
		Given I am admin user

		When I try to retrieve a project

		Then I get status code 200

	Scenario: retrieve project as normal user
		Given I am normal user

		When I try to retrieve a project

		Then I get status code 200

	Scenario: retrieve project as anonymous user
		Given I am anonymous user

		When I try to retrieve a project

		Then I get status code 403
