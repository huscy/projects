import pytest
from itertools import cycle
from model_bakery import baker

from huscy.projects.models import Membership
from huscy.projects.services import set_principal_investigators

pytestmark = pytest.mark.django_db


def test_set_principal_investigators(django_user_model, project):
    users = baker.make(django_user_model, _quantity=3)
    assert project.principal_investigators.count() == 0

    baker.make(Membership, project=project, user=cycle(users),
               role=Membership.Roles.PROJECT_COORDINATOR, is_coordinator=True, _quantity=3)

    set_principal_investigators(project, users)
    assert project.principal_investigators.count() == len(users)


def test_set_principal_investigators_with_normal_project_member(project, user):
    assert not project.principal_investigators.filter(user=user).exists()

    baker.make(Membership, project=project, user=user, is_coordinator=False)

    with pytest.raises(ValueError) as e:
        set_principal_investigators(project, [user])

    assert str(e.value) == ('Only project members who are coordinators can become '
                            'principal investigators.')
    project.refresh_from_db()
    assert not project.principal_investigators.filter(user=user).exists()


def test_set_principal_investigators_with_non_project_member(project, user):
    assert not project.principal_investigators.filter(user=user).exists()

    with pytest.raises(ValueError) as e:
        set_principal_investigators(project, [user])

    assert str(e.value) == ('Only project members who are coordinators can become '
                            'principal investigators.')
    project.refresh_from_db()
    assert not project.principal_investigators.filter(user=user).exists()
