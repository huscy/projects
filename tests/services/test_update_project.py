import pytest
from model_bakery import baker

from django.db.utils import IntegrityError

from huscy.projects.services import update_project

pytestmark = pytest.mark.django_db


def test_update_project(project):
    result = update_project(project, 123456, 'new title', 'new description')

    project.refresh_from_db()

    assert result == project
    assert project.local_id == 123456
    assert project.title == 'new title'
    assert project.description == 'new description'


def test_update_description(project):
    update_project(project, description='new description')

    project.refresh_from_db()

    assert project.description == 'new description'


def test_update_local_id(project):
    update_project(project, local_id=999)

    project.refresh_from_db()

    assert project.local_id == 999


def test_update_local_id_to_existing_one(project):
    baker.make('projects.Project', research_unit=project.research_unit, local_id=111)

    with pytest.raises(IntegrityError):
        update_project(project, local_id=111)


def test_update_title(project):
    update_project(project, title='new title')

    project.refresh_from_db()

    assert project.title == 'new title'
