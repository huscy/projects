Feature: create projects

	Scenario: create project as admin user
		Given I am admin user

		When I try to create a project

		Then I get status code 201

	Scenario: create project as staff user
		Given I am staff user

		When I try to create a project

		Then I get status code 201

	Scenario: create project as normal user
		Given I am normal user

		When I try to create a project

		Then I get status code 201

	Scenario: create project as anonymous user
		Given I am anonymous user

		When I try to create a project

		Then I get status code 403

	Scenario: create project with invalid local id
		Given I am normal user

		When I try to create a project with invalid local id

		Then I get status code 400
		And I get error message that local id must be a valid integer

	Scenario: create project with already taken local id
		Given I am normal user
		And one project with local id 123

		When I try to create a project with local id 123

		Then I get status code 400
		And I get error message that research unit and local id must make a unique set
