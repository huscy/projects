Feature: create memberships

	Scenario: create membership as admin user
		Given I am admin user

		When I try to create a membership

		Then I get status code 201

	Scenario: create membership as staff user
		Given I am staff user

		When I try to create a membership

		Then I get status code 403

	Scenario: create membership with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to create a membership

		Then I get status code 403

	Scenario: create membership as project coordinator
		Given I am normal user
		And I am project coordinator

		When I try to create a membership

		Then I get status code 201

	Scenario: create membership as project member with write permission
		Given I am normal user
		And I am project member with write permission

		When I try to create a membership

		Then I get status code 403

	Scenario: create membership as project member with read permission
		Given I am normal user
		And I am project member with read permission

		When I try to create a membership

		Then I get status code 403

	Scenario: create membership as normal user (without project membership)
		Given I am normal user

		When I try to create a membership

		Then I get status code 403

	Scenario: create membership as anonymous user
		Given I am anonymous user

		When I try to create a membership

		Then I get status code 403
