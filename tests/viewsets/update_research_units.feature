Feature: update research units

	Scenario: update research unit as admin user
		Given I am admin user

		When I try to update a research unit

		Then I get status code 200

	Scenario: partial update research unit as admin user
		Given I am admin user

		When I try to partial update a research unit

		Then I get status code 200

	Scenario: update research unit with change_researchunit permission
		Given I am admin user
		And I have change_researchunit permission

		When I try to update a research unit

		Then I get status code 200

	Scenario: partial update research unit with change_researchunit permission
		Given I am admin user
		And I have change_researchunit permission

		When I try to partial update a research unit

		Then I get status code 200

	Scenario: update research unit as normal user
		Given I am normal user

		When I try to update a research unit

		Then I get status code 403

	Scenario: partial update research unit as normal user
		Given I am normal user

		When I try to partial update a research unit

		Then I get status code 403

	Scenario: update research unit as anonymous user
		Given I am anonymous user

		When I try to update a research unit

		Then I get status code 403

	Scenario: partial update research unit as anonymous user
		Given I am anonymous user

		When I try to partial update a research unit

		Then I get status code 403
