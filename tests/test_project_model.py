import pytest
from model_bakery import baker

from huscy.projects.models import Membership


@pytest.mark.django_db
def test_principal_investigators_property(project):
    principal_investigators = baker.make(Membership, project=project,
                                         role=Membership.Roles.PRINCIPAL_INVESTIGATOR, _quantity=2)
    baker.make(Membership, role=Membership.Roles.PRINCIPAL_INVESTIGATOR, _quantity=2)
    baker.make(Membership, project=project, role=Membership.Roles.PROJECT_COORDINATOR)
    baker.make(Membership, project=project, role=Membership.Roles.READ_WRITE_MEMBERSHIP)
    baker.make(Membership, project=project, role=Membership.Roles.READ_ONLY_MEMBERSHIP)

    assert list(project.principal_investigators) == principal_investigators
