Feature: delete projects

	Scenario: delete project as admin user
		Given I am admin user

		When I try to delete a project

		Then I get status code 204

	Scenario: delete project as staff user
		Given I am staff user

		When I try to delete a project

		Then I get status code 403

	Scenario: delete project with delete_project permission
		Given I am normal user
		And I have delete_project permission

		When I try to delete a project

		Then I get status code 204

	Scenario: delete project as project coordinator
		Given I am normal user
		And I am project coordinator

		When I try to delete a project

		Then I get status code 403

	Scenario: delete project as project member with write permission
		Given I am normal user
		And I am project member with write permission

		When I try to delete a project

		Then I get status code 403

	Scenario: delete project as project member with read permission
		Given I am normal user
		And I am project member with read permission

		When I try to delete a project

		Then I get status code 403

	Scenario: delete project as normal user
		Given I am normal user

		When I try to delete a project

		Then I get status code 403

	Scenario: delete project as anonymous user
		Given I am anonymous user

		When I try to delete a project

		Then I get status code 403
