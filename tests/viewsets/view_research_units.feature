Feature: view research units

	Scenario: list research units as admin user
		Given I am admin user

		When I try to list research units

		Then I get status code 200

	Scenario: retrieve research units as admin user
		Given I am admin user

		When I try to retrieve a research unit

		Then I get status code 200

	Scenario: list research units as normal user
		Given I am normal user

		When I try to list research units

		Then I get status code 200

	Scenario: retrieve research units as normal user
		Given I am normal user

		When I try to retrieve a research unit

		Then I get status code 200

	Scenario: list research units as anonymous user
		Given I am anonymous user

		When I try to list research units

		Then I get status code 403

	Scenario: retrieve research units as anonymous user
		Given I am anonymous user

		When I try to retrieve a research unit

		Then I get status code 403
