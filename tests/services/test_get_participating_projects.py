from model_bakery import baker

from huscy.projects.models import Membership
from huscy.projects.services import (create_membership, get_participating_projects)


def test_no_project_participation(admin_user):
    users_project = get_participating_projects(admin_user).values_list('id', flat=True)
    assert list(users_project) == []


def test_pi_participated_projects(user, admin_user, project):
    baker.make(Membership, project=project, user=user, role=Membership.Roles.PRINCIPAL_INVESTIGATOR)
    user_projects = get_participating_projects(user).values_list('id', flat=True)
    assert list(user_projects) == [project.id]


def test_member_projects(user, project):
    user_projects = get_participating_projects(user).values_list('id', flat=True)
    assert list(user_projects) == []

    create_membership(project, user)

    user_projects = get_participating_projects(user).values_list('id', flat=True)
    assert list(user_projects) == [project.id]


def test_pi_and_member_projects(user):
    pi_project = baker.make('projects.Project')
    baker.make(Membership, project=pi_project, user=user,
               is_coordinator=True, role=Membership.Roles.PRINCIPAL_INVESTIGATOR)

    member_project = baker.make('projects.Project')
    baker.make(Membership, project=member_project, user=user)

    user_projects = get_participating_projects(user).values_list('id', flat=True)
    assert sorted(user_projects) == sorted([pi_project.id, member_project.id])
