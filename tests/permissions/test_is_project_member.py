import pytest

from django.test import RequestFactory

from huscy.projects.permissions import IsProjectMember
from huscy.projects.services import create_membership
from huscy.projects.views import ProjectViewSet

pytestmark = pytest.mark.django_db

request_factory = RequestFactory()


@pytest.fixture
def _request(user):
    request = request_factory.get('/any/url/')
    request.user = user
    return request


def test_with_non_project_member(project, user, _request):
    view = ProjectViewSet.as_view({'get': 'list'})(_request)
    view.project = project

    assert IsProjectMember().has_permission(_request, view) is False


def test_with_project_member(project, user, _request):
    view = ProjectViewSet.as_view({'get': 'list'})(_request)
    view.project = project

    create_membership(project, user)

    assert IsProjectMember().has_permission(_request, view) is True
