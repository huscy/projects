Feature: set principal investigator

	Background:
		Given one project
		And one project member as coordinator

	Scenario: set principal investigator as admin user
		Given I am admin user

		When I try to set the principal investigator

		Then I get status code 200

	Scenario: set principal investigator as staff user
		Given I am staff user

		When I try to set the principal investigator

		Then I get status code 403

	Scenario: set principal investigator with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to set the principal investigator

		Then I get status code 403

	Scenario: set principal investigator as project coordinator
		Given I am normal user
		And I am project coordinator

		When I try to set the principal investigator

		Then I get status code 200

	Scenario: set principal investigator as project member with write permission
		Given I am normal user
		And I am project member with write permission

		When I try to set the principal investigator

		Then I get status code 403

	Scenario: set principal investigator as project member with read permission
		Given I am normal user
		And I am project member with read permission

		When I try to set the principal investigator

		Then I get status code 403

	Scenario: set principal investigator as normal user
		Given I am normal user

		When I try to set the principal investigator

		Then I get status code 403

	Scenario: set principal investigator as anonymous user
		Given I am anonymous user

		When I try to set the principal investigator

		Then I get status code 403

	Scenario: set a normal team member as principal investigator
		Given I am normal user
		And I am project coordinator

		When I try to set normal team member as principal investigator

		Then I get status code 400
		And I get error message that only project coordinators can become principal investigators

	Scenario: set any user as principal investigator
		Given I am normal user
		And I am project coordinator

		When I try to set any user as principal investigator

		Then I get status code 400
		And I get error message that only project coordinators can become principal investigators
