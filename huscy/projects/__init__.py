# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (1, 7, 2)

__version__ = '.'.join(str(x) for x in VERSION)
