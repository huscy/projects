from datetime import timedelta

import pytest
from model_bakery import baker
from pytest_bdd import given
from rest_framework.test import APIClient

from huscy.projects.models import Project


ONE_HOUR = timedelta(hours=1)


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Phil', last_name='Stift')


@pytest.fixture
def admin_client(admin_user):
    client = APIClient()
    client.login(username=admin_user.username, password='password')
    return client


@pytest.fixture
def client(user):
    client = APIClient()
    client.login(username=user.username, password='password')
    return client


@pytest.fixture
def anonymous_client():
    return APIClient()


@pytest.fixture
@given('one research unit')
def research_unit():
    return baker.make('projects.ResearchUnit')


@pytest.fixture
@given('one project')
def project(research_unit):
    return baker.make(Project, research_unit=research_unit)


@pytest.fixture
def membership(project):
    return baker.make('projects.Membership', project=project)
