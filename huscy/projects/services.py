from django.db.transaction import atomic
from guardian.shortcuts import assign_perm, remove_perm

from huscy.projects.models import Membership, Project, ResearchUnit


@atomic
def create_membership(project, user, is_principal_investigator=False,
                      is_coordinator=False, has_write_permission=False):
    assign_perm('view_project', user, project)
    if any([is_principal_investigator, is_coordinator, has_write_permission]):
        assign_perm('change_project', user, project)

    if is_principal_investigator is True:
        role = Membership.Roles.PRINCIPAL_INVESTIGATOR
    elif is_coordinator is True:
        role = Membership.Roles.PROJECT_COORDINATOR
    elif has_write_permission is True:
        role = Membership.Roles.READ_WRITE_MEMBERSHIP
    else:
        role = Membership.Roles.READ_ONLY_MEMBERSHIP

    return Membership.objects.create(
        project=project,
        user=user,
        role=role,
        is_coordinator=is_coordinator,
    )


@atomic
def create_project(title, research_unit, principal_investigators, creator,
                   local_id=None, description=''):
    if local_id is None:
        local_id = Project.objects.get_next_local_id(research_unit)

    project = Project.objects.create(
        description=description,
        local_id=local_id,
        research_unit=research_unit,
        title=title,
    )

    for principal_investigator in principal_investigators:
        create_membership(project, principal_investigator, is_principal_investigator=True)

    if creator not in principal_investigators:
        create_membership(project, creator, is_coordinator=True)

    return project


@atomic
def delete_membership(membership):
    remove_perm('view_project', membership.user, membership.project)
    remove_perm('change_project', membership.user, membership.project)
    membership.delete()


@atomic
def delete_project(project):
    map(delete_membership, project.membership_set.all())
    project.delete()


def get_memberships(project):
    return Membership.objects.filter(project=project)


def get_participating_projects(user):
    return Project.objects.filter(membership__user=user)


def get_projects():
    return Project.objects.all()


def get_research_units():
    return ResearchUnit.objects.all()


@atomic
def set_principal_investigators(project, users):
    if len(users) == 0:
        raise ValueError('Please assign at least one Principal investigator to proceed.')

    Membership.objects.filter(
        project=project, role=Membership.Roles.PRINCIPAL_INVESTIGATOR
    ).update(role=Membership.Roles.PROJECT_COORDINATOR)

    project_coordinator_count = Membership.objects.filter(
        user__in=users, project=project, role=Membership.Roles.PROJECT_COORDINATOR).count()
    if project_coordinator_count != len(users):
        raise ValueError('Only project members who are coordinators can become '
                         'principal investigators.')

    Membership.objects.filter(
        project=project, user__in=users
    ).update(role=Membership.Roles.PRINCIPAL_INVESTIGATOR)

    return project


def update_membership(membership, is_coordinator, has_write_permission):
    if is_coordinator or has_write_permission:
        assign_perm('change_project', membership.user, membership.project)
    else:
        remove_perm('change_project', membership.user, membership.project)
    membership.is_coordinator = is_coordinator
    membership.save()
    return membership


def update_project(project, local_id=None, title=None, description=None):
    update_fields = []
    if local_id is not None:
        project.local_id = local_id
        update_fields.append('local_id')
    if title is not None:
        project.title = title
        update_fields.append('title')
    if description is not None:
        project.description = description
        update_fields.append('description')
    project.save(update_fields=update_fields)
    return project
