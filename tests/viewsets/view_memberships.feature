Feature: view memberships

	Scenario: retrieve is not allowed
		Given I am admin user

		When I try to retrieve a membership

		Then I get status code 405

	Scenario: list memberships as admin user
		Given I am admin user

		When I try to list memberships

		Then I get status code 200

	Scenario: list memberships as staff user
		Given I am staff user

		When I try to list memberships

		Then I get status code 200

	Scenario: list memberships as normal user (without project membership)
		Given I am normal user

		When I try to list memberships

		Then I get status code 200

	Scenario: list memberships as anonymous user
		Given I am anonymous user

		When I try to list memberships

		Then I get status code 403
