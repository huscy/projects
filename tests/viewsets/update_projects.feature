Feature: update projects

	Scenario: update project as admin user
		Given I am admin user

		When I try to update the project

		Then I get status code 200

	Scenario: partial update project as admin user
		Given I am admin user

		When I try to partial update the project title

		Then I get status code 200

	Scenario: update project as staff user
		Given I am staff user

		When I try to update the project

		Then I get status code 403

	Scenario: partial update project as staff user
		Given I am staff user

		When I try to partial update the project title

		Then I get status code 403

	Scenario: update project with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to update the project

		Then I get status code 200

	Scenario: partial update project title with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to partial update the project title

		Then I get status code 200

	Scenario: partial update project description with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to partial update the project description

		Then I get status code 200

	Scenario: partial update project research unit with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to partial update the project research unit

		Then I get status code 200

	Scenario: partial update project local id with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to partial update the project local id

		Then I get status code 200

	Scenario: partial update principal investigators with change_project permission
		Given I am normal user
		And I have change_project permission
		And one project member as coordinator

		When I try to partial update the project principal investigators

		Then I get status code 200

	Scenario: update project as project coordinator
		Given I am normal user
		And I am project coordinator

		When I try to update the project

		Then I get status code 200

	Scenario: partial update project as project coordinator
		Given I am normal user
		And I am project coordinator

		When I try to partial update the project title

		Then I get status code 200

	Scenario: update project as project member with write permission
		Given I am normal user
		And I am project member with write permission

		When I try to update the project

		Then I get status code 200

	Scenario: partial update project as project member with write permission
		Given I am normal user
		And I am project member with write permission

		When I try to partial update the project title

		Then I get status code 200

	Scenario: update project as project member with read permission
		Given I am normal user
		And I am project member with read permission

		When I try to update the project

		Then I get status code 403

	Scenario: partial update project as project member with read permission
		Given I am normal user
		And I am project member with read permission

		When I try to partial update the project title

		Then I get status code 403

	Scenario: update project as normal user
		Given I am normal user

		When I try to update the project

		Then I get status code 403

	Scenario: partial update project as normal user
		Given I am normal user

		When I try to partial update the project title

		Then I get status code 403

	Scenario: update project as anonymous user
		Given I am anonymous user

		When I try to update the project

		Then I get status code 403

	Scenario: partial update project as anonymous user
		Given I am anonymous user

		When I try to partial update the project title

		Then I get status code 403

	Scenario: update project local id to already taken value
		Given I am normal user
		And I am project coordinator
		And one project with local id 123

		When I try to update the project local id to 123

		Then I get status code 400
		And I get error message that local id already exists
