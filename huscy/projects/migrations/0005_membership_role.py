from django.db import migrations, models


def forward_function(apps, schema_editor):
    Membership = apps.get_model('projects', 'Membership')
    memberships = []
    for membership in Membership.objects.select_related('project').all():
        if membership.user_id == membership.project.principal_investigator_id:
            membership.role = Membership.Role.PRINCIPAL_INVESTIGATOR
        elif membership.is_coordinator is True:
            membership.role = Membership.Role.PROJECT_COORDINATOR
        elif membership.has_write_permission:
            membership.role = Membership.Role.READ_WRITE_MEMBERSHIP
        memberships.append(membership)
    Membership.objects.bulk_update(memberships, ['role'])


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0004_remove_project_project_manager"),
    ]

    operations = [
        migrations.AddField(
            model_name="membership",
            name="role",
            field=models.CharField(
                choices=[
                    ("pi", "Principal investigator"),
                    ("pc", "Project coordinator"),
                    ("rwm", "Read-write membership"),
                    ("rom", "Read-only membership"),
                ],
                default="rom",
                max_length=4,
                verbose_name="Role",
            ),
        ),
        migrations.RunPython(forward_function),
        migrations.RemoveField(
            model_name="project",
            name="principal_investigator",
        ),
    ]
